<?php get_header(); ?>

<?php 
$big_thumbnail = get_field('big_thumbnail_for_full_single_post');
?>
<!-- page begin  -->
  <div class="main-content-wrapper main-area">
    <div class="container">
      <div class="blog">
        <h1 class="main-title"><?php the_title(); ?></h1>
        <div class="grey-line"></div>
        <?php get_template_part('page-nav'); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>    
          <div class="post type-post single-post">
            <?php if ( $big_thumbnail ) : ?>
              <img class="big-single-thumbnails" src="<?php echo $big_thumbnail; ?>">
             <?php else : ?>
              <img class="big-single-thumbnails" src="<?php bloginfo( 'template_url' ); ?>/img/big/post-sample-640x360.jpg">
            <?php endif; ?>

            
            
          	<div class="article-header">

          	  <?php get_template_part('blog_meta_info'); ?>
          	</div>
          	<?php the_content(); ?>
          </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div> 
      </div>
      
      <div class="comments-template">
        <?php comments_template(); ?>
      </div>

  <?php get_footer(); ?>

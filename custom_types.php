<?php 

# Add post-type gallery
function create_gallery() {
  register_post_type( 'gallery',
    array(
      'labels' => array(
        'name' => __( 'Gallery' ),
        'singular_name' => __( 'Gallery post' )
      ),
      'menu_position' => 5,
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'gallery'),
      'menu_icon'   => 'dashicons-format-gallery',
    )
  );
}
add_action( 'init', 'create_gallery' );

# Add post-type works
function create_works() {
  register_post_type( 'works',
    array(
      'labels' => array(
        'name' => __( 'Works' ),
        'singular_name' => __( 'Work' ),
        'new_item' => 'Add new work',
        'add_new' => 'Add new work',
        'add_new_item' => 'Work name',
      ),
      'menu_position' => 6,
      'public' => true,
      'taxonomies' => array('All'),
      'has_archive' => true,
      'rewrite' => array('slug' => 'work'),
      'menu_icon'   => 'dashicons-grid-view',
      'supports' => array('title','editor','comments'),
    )
  );
}
add_action( 'init', 'create_works' );

# Add taxonomy for works post types
function create_portfolio_tax() {
  register_taxonomy(
    'works_list',
    'works',
    array(
      'label' => __( 'Works list' ),
      'rewrite' => array( 'slug' => 'works-list' ),
      'hierarchical' => true,
    )
  );
}
add_action( 'init', 'create_portfolio_tax' );


// # Add categories in custom works taxonomy
// function create_cat_all_works() {
  
//   wp_insert_term( 'All', 'works_list', array(
//     'description'=> 'Default for all your works',
//     'slug'=>sanitize_title( 'all_works' ),
//   ));
  
// }
// add_action( 'init', 'create_cat_all_works' );

// # Set 'All' default for works taxonomy
// function mfields_set_default_object_terms( $post_id, $post ) {
//     if ( 'publish' === $post->post_status ) {
//         $defaults = array(
//             'posttypes' => array( 'works' )
//             );
//         $taxonomies = get_object_taxonomies( $post->post_type );
//         foreach ( (array) $taxonomies as $taxonomy ) {
//             $terms = wp_get_post_terms( $post_id, $taxonomy );
//             if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
//                 wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
//             }
//         }
//     }
// }
// add_action( 'save_post', 'mfields_set_default_object_terms', 100, 2 ); 
<div class="blog-meta-information">
  <div class="blog-meta-date"><i class="fa fa-calendar"></i><?php echo mb_strtolower( get_the_date('d F') ); ?></div>
  <div class="blog-meta-username"><i class="fa fa-user"></i><?php the_author(); ?></div>
  <div class="blog-meta-date"><i class="fa fa-comments"></i><?php echo comments_number(); ?></div>
</div>
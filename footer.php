<?php
$copyright_text = getOptionsField('copyright_text');
$activate_social = getOptionsField('activate_social');
$social_icon_1 = getOptionsField('social_icon_1');
$social_link_1 = getOptionsField('social_link_1');
$social_icon_2 = getOptionsField('social_icon_2');
$social_link_2 = getOptionsField('social_link_2');
$social_icon_3 = getOptionsField('social_icon_3');
$social_link_3 = getOptionsField('social_link_3');
$social_icon_4 = getOptionsField('social_icon_4');
$social_link_4 = getOptionsField('social_link_4');


?>
    <!-- footer begin  -->
      <div class="footer-area">
        <div class="container">
          <div class="footer">
            <?php wp_footer(); ?>
            <div class="menee">
              <?php 
              $args = array(
                'order'                  => 'ASC'
                ,'orderby'                => 'menu_order'
                ,'post_type'              => 'nav_menu_item'
                ,'post_status'            => 'publish'
                ,'output'                 => ARRAY_A
                ,'output_key'             => 'menu_order'
                ,'nopaging'               => true
                ,'update_post_term_cache' => false 
              );
              pr(wp_get_nav_menu_items( '0' )); ?>
            </div>
            <?php if ( $copyright_text ) : ?>
              <p class="footer-left-box"><?php echo $copyright_text; ?></p>
              <?php else : ?>
              <p class="footer-left-box">© 2014 Robb Armstrong,  All Rights Reserved</p>
            <?php endif; ?>
            
            <?php if ( $activate_social ) : ?>
              <p class="footer-right-box">
                
                <?php if ( $social_link_1 ) : ?>
                  <a target="_blank" class="fa <?php echo $social_icon_1; ?>" href="<?php echo $social_link_1; ?>"></a>
                <?php else : ?>
                  <a target="_blank" class="fa fa-facebook" href="#"></a>
                <?php endif; ?>
                
                <?php if ( $social_link_2 ) : ?>
                  <a target="_blank" class="fa <?php echo $social_icon_2; ?>" href="<?php echo $social_link_2; ?>"></a>
                <?php else : ?>
                  <a target="_blank" class="fa fa-twitter" href="#"></a>
                <?php endif; ?>
                
                <?php if ( $social_link_3 ) : ?>
                  <a target="_blank" class="fa <?php echo $social_icon_3; ?>" href="<?php echo $social_link_3; ?>"></a>
                <?php else : ?>
                  <a target="_blank" class="fa fa-dribbble" href="#"></a>
                <?php endif; ?>
                
                <?php if ( $social_link_4 ) : ?>
                  <a target="_blank" class="fa <?php echo $social_icon_4; ?>" href="<?php echo $social_link_4; ?>"></a>
                <?php else : ?>
                  <a target="_blank" class="fa fa-pinterest" href="#"></a>
                <?php endif; ?>
              </p>
            <?php endif; ?>
            
          </div>
        </div>
      </div>
    <!-- footer end  -->
    </div>
  <!-- .main-content-wrapper end  -->
  </div> <!-- .main-container -->
</body>
</html>
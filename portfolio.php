<?php 
/*
Template Name: Work
*/
?>
<?php 
// pr( is_page_template( 'portfolio.php' ) ); exit; ?>
<?php 

?>
<?php get_header(); ?>

<!-- portfolio begin  -->
  <div class="main-content-wrapper main-area">
	<div class="container">
	  <div class="blog">
		<h1 class="main-title"><?php the_title(); ?></h1>
		<div class="grey-line"></div>
		<?php get_template_part('page-nav'); ?>
		
		<?php 
		$terms = get_terms("works_list");
		 $count = count($terms);
		 if($count > 0){
		 	echo '<ul class="works-navigation"><li class="all"><a href="#">All</a></li>';
			 foreach ($terms as $term) {
			 	echo '<li class="' . strtolower($term->name) . '"><a href="#">'.$term->name.'</a></li>';

			 }
			 echo '</ul>';
		 } ?>
			<div class="post type-post single-post works-post">
				
				<?php $args = array ( 'post_type' => 'works' );
				$works = new WP_Query( $args ); ?>

				<?php if ($works->have_posts()) : while ($works->have_posts()) : $works->the_post(); ?>
					<?php $term_list = wp_get_post_terms($post->ID, 'works_list', array("fields" => "all")); ?>
					
						
						<?php 
							$small_image_of_your_work = get_field('small_image_of_your_work');
							$big_image_of_your_work = get_field('big_image_of_your_work');
							$short_description_of_your_work = get_field('short_description_of_your_work');
						?>

						<div class="work-box all <?php foreach ($term_list as $value) { echo $value->slug  . ' ' ; } ?>">
							<a class="work-link-to big" href="<?php the_permalink(); ?>">
								<img class="work-thumbnail" src="<?php echo $small_image_of_your_work; ?>">
								<div class="work-info-box">
									<h3 class="work-name">
										<?php the_title(); ?><br>
										<span class="work-description"><?php echo $short_description_of_your_work; ?></span>
									</h3><br>
								</div>
							</a>
						</div>
			  		
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div> 
	  </div>
  <?php get_footer(); ?>

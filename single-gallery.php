<?php get_header(); ?>

<?php 
$big_thumbnail = get_field('big_thumbnail_for_full_single_post');
?>

<!-- gallery-single begin  -->
  <div class="main-content-wrapper main-area">
    <div class="container">
      <div class="blog simple-gallery">
        <h1 class="main-title">Blog</h1>
        <div class="grey-line"></div>
        <?php get_template_part('page-nav'); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>    
          <div class="post type-post single-post">
            <?php if ( $big_thumbnail ) : ?>
              <img class="big-single-thumbnails" src="<?php echo $big_thumbnail; ?>">
            <?php endif; ?>

            
            
          	<div class="article-header">
          	  
              <h1><?php the_title(); ?></h1>

          	  <?php get_template_part('blog_meta_info'); ?>
          	</div>
          	<?php the_content(); ?>
          </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div> 
      </div>
      
      <div class="comments-template">
        <?php comments_template(); ?>
      </div>

  <?php get_footer(); ?>

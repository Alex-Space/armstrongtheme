<?php

# debug tool
function pr($data) {
  echo '<pre>';
	print_r($data);
  echo '</pre>';
}

# require files
require_once(get_stylesheet_directory() . '/custom_types.php'); // Include all custom post types
require_once(get_stylesheet_directory() . '/options_page.php'); // Include Theme settings page
require_once(get_stylesheet_directory() . '/acf_settings.php'); // Include ACF settings
require_once(get_stylesheet_directory() . '/require/class-tgm-plugin-activation.php'); // Include the TGM_Plugin_Activation class.
require_once(get_stylesheet_directory() . '/plugin_activate.php'); // Include the TGM_Plugin_Activation config

# Remove gallery core styles
function clear_gallery_styles() {
  add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'clear_gallery_styles' );

# Register exif_thumbnail
function custom_theme_setup() {
  add_theme_support('post-thumbnails', array('post', 'gallery'));
}
add_action( 'after_setup_theme', 'custom_theme_setup' );

function load_styles_and_scripts() {
  //wp_deregister_script('jquery');
  wp_register_script( 'main_scripts', get_template_directory_uri() . '/js/main.js');
  wp_register_script( 'bxslider_scripts', get_template_directory_uri() . '/js/jquery.bxslider.js', array('jquery'));
  

  wp_register_style( 'bxslider_styles', get_template_directory_uri() . '/css/jquery.bxslider.css');
  wp_register_style( 'admin', get_template_directory_uri() . '/css/admin.css');
  wp_register_style( 'main_styles', get_template_directory_uri() . '/css/theme_style.css');
  wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
  wp_register_style( 'opensans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,500,800,700,300&subset=cyrillic,cyrillic-ext,latin,latin-ext');
  
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'main_scripts' );
  wp_enqueue_script( 'bxslider_scripts' );
  wp_enqueue_style( 'opensans' );
  wp_enqueue_style( 'font-awesome' );
  
  wp_enqueue_style( 'bxslider_styles' );
  
  if ( ! is_admin() ) {
	wp_enqueue_style( 'main_styles' );
  }

  if ( is_admin() ) {
	wp_enqueue_style( 'admin' );
  }

  $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );

  if ( $template_name = 'portfolio.php' ) {
      wp_enqueue_script( 'shuffle_scripts' );
    }
}
add_action('init', 'load_styles_and_scripts');

function register_my_menu() {
  register_nav_menus( array(
	'header_menu' => 'Sidebar menu'
  ) );
}
add_action( 'init', 'register_my_menu' );


function create_sidebar_menu() {
  // Check if the menu exists
  $menu_exists = wp_get_nav_menu_object( $menu_name );
  
  if( !$menu_exists){
      $menu_id = wp_create_nav_menu( 'Sidebar menu' );

      $menu = array( 'menu-item-type' => 'custom', 'menu-item-url' => get_home_url('/'),'menu-item-title' => 'Home', 'menu-item-classes' => 'fa fa-facebook' );
    wp_update_nav_menu_item( $menu_id, 0, $menu );
  }
}
add_action( 'init', 'create_sidebar_menu' );


function add_thumb() {
  add_post_type_support('gallery', array('thumbnail') );
}
add_action( 'init', 'add_thumb' );

# Remove not necessary fields in templates
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;

  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
	
	if($template_file == 'blog.php'){ // edit the template name
	  remove_post_type_support('page', 'editor');
	}
}
add_action( 'admin_init', 'hide_editor' );

function get_page_navigation_styles() {
  global $wp_query;
  $post_id = $wp_query->post->ID;
  $post = get_post( $post_id );
  $slug = $post->post_name;
  $all_elem_info = wp_get_nav_menu_items('sidebar-menu'); 
  $current_menu_id = ''; 

  foreach ($all_elem_info as $key => $value) {
    $classes_array = $value;
    
    if ( $value->object_id == get_the_ID() ) {
      $current_menu_id = $key;      
    }
  }

  $next = $all_elem_info[ $current_menu_id + 1 ];
  $prev = $all_elem_info[ $current_menu_id - 1 ];

  if ( $prev == false ) {
    echo '<style>
            .prev-page-link {
              opacity: 0.2;
              cursor: default;
            }
            .prev-page-link:hover {
              opacity: 0.2;
            }
          </style>';
    echo "<script>
            jQuery(document).ready(function($) {
              $('.navigation-link-box').on('click', '.prev-page-link', function(event) {
                event.preventDefault();
              });
            });
          </script>";
  } elseif ( $next == false ) {
    echo '<style>
            .next-page-link {
              opacity: 0.2;
              cursor: default;
            }
            .next-page-link:hover {
              opacity: 0.2;
            }
          </style>';
    echo "<script>
            jQuery(document).ready(function($) {
              $('.navigation-link-box').on('click', '.next-page-link', function(event) {
                event.preventDefault();
              });
            });
          </script>";

  }
}

function get_page_navigation( $link ) {
  
  if ( !is_admin() ) {
    global $wp_query;
    $post_id = $wp_query->post->ID;
    $post = get_post( $post_id );
    $slug = $post->post_name;
    $all_elem_info = wp_get_nav_menu_items('sidebar-menu'); 
    $current_menu_id = ''; 

    foreach ($all_elem_info as $key => $value) {
      $classes_array = $value;
      
      if ( $value->object_id == get_the_ID() ) {
        $current_menu_id = $key;      
      }
    }

    $next = $all_elem_info[ $current_menu_id + 1 ];
    $prev = $all_elem_info[ $current_menu_id - 1 ];
    
      if ( $link == 'next' ) {
        return $next->url;
      } elseif ( $link == 'prev' ) {
        return $prev->url;
      }
  }
}
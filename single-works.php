<?php get_header(); ?>
<?php 
$big_image_of_your_work_1 = get_field('big_image_of_your_work_1');
$big_image_of_your_work_2 = get_field('big_image_of_your_work_2');
$big_image_of_your_work_3 = get_field('big_image_of_your_work_3');
$full_description_about_your_work_1 = get_field('full_description_about_your_work_1');
?>

<!-- works-single begin  -->
  <div class="main-content-wrapper main-area">
    <div class="container">
      <div class="blog">
        <h1 class="main-title"><?php the_title(); ?></h1>
        <div class="grey-line"></div>
        <?php get_template_part('page-nav'); ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>    
          <div class="post type-post single-post">
            <?php if ( $big_image_of_your_work_1 ) : ?>
              <img class="big-single-thumbnails" src="<?php echo $big_image_of_your_work_1; ?>">
            <?php endif; ?>
            
          	<div class="article-header">
          	  
              <h1><?php the_title(); ?></h1>

          	  <?php get_template_part('blog_meta_info'); ?>
          	</div>
          	
            <div class="work-full-text"><?php echo $full_description_about_your_work_1; ?></div>
            
            <div class="bottom-work-images">
              <?php if ( $big_image_of_your_work_2 ) : ?>
                <img class="big-single-thumbnails" src="<?php echo $big_image_of_your_work_2; ?>">
              <?php endif; ?>

              <?php if ( $big_image_of_your_work_3 ) : ?>
                <img class="big-single-thumbnails" src="<?php echo $big_image_of_your_work_3; ?>">
              <?php endif; ?>
            </div>

          </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div> 
      </div>
      
      <div class="comments-template">
        <?php comments_template(); ?>
      </div>

  <?php get_footer(); ?>

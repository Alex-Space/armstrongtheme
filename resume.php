<?php 
/*
Template Name: Resume
*/
?>
<?php 

// Education section

$activate_education = get_field( 'activate_education' );
$first_education_place = get_field( 'first_education_place' );
$years_you_studied = get_field( 'years_you_studied' );
$description_1 = get_field( 'description_1' );

$second_education_place = get_field( 'second_education_place' );
$years_you_studied_2 = get_field( 'years_you_studied_2' );
$description_2 = get_field( 'first_education_place' );

$third_education_place = get_field( 'third_education_place' );
$years_you_studied_3 = get_field( 'years_you_studied_3' );
$description_3 = get_field( 'description_3' );

$fourth_education_place = get_field( 'fourth_education_place' );
$years_you_studied_4 = get_field( 'years_you_studied_4' );
$description_4 = get_field( 'description_4' );

$fifth_education_place = get_field( 'fifth_education_place' );
$years_you_studied_5 = get_field( 'years_you_studied_5' );
$description_5 = get_field( 'description_5' );

// Skills section

$skills_activate = get_field( 'skills_activate' );
$skill_1 = get_field( 'skill_1' );
$skill_level_in_percent_value_1 = get_field( 'skill_level_in_percent_value_1' );
$skill_2 = get_field( 'skill_2' );
$skill_level_in_percent_value_2 = get_field( 'skill_level_in_percent_value_2' );
$skill_3 = get_field( 'skill_3' );
$skill_level_in_percent_value_3 = get_field( 'skill_level_in_percent_value_3' );
$skill_4 = get_field( 'skill_4' );
$skill_level_in_percent_value_4 = get_field( 'skill_level_in_percent_value_4' );
$skill_5 = get_field( 'skill_5' );
$skill_level_in_percent_value_5 = get_field( 'skill_level_in_percent_value_5' );
$skill_6 = get_field( 'skill_6' );
$skill_level_in_percent_value_6 = get_field( 'skill_level_in_percent_value_6' );
$skill_7 = get_field( 'skill_7' );
$skill_level_in_percent_value_7 = get_field( 'skill_level_in_percent_value_7' );
$skill_8 = get_field( 'skill_8' );
$skill_level_in_percent_value_8 = get_field( 'skill_level_in_percent_value_8' );
$skill_9 = get_field( 'skill_9' );
$skill_level_in_percent_value_9 = get_field( 'skill_level_in_percent_value_9' );
$skill_10 = get_field( 'skill_10' );
$skill_level_in_percent_value_10 = get_field( 'skill_level_in_percent_value_10' );

// Services

$activate_services = get_field( 'activate_services' );
$icon_1 = get_field( 'icon_1' );
$name_of_service_1 = get_field( 'name_of_service_1' );
$icon_2 = get_field( 'icon_2' );
$name_of_service_2 = get_field( 'name_of_service_2' );
$icon_3 = get_field( 'icon_3' );
$name_of_service_3 = get_field( 'name_of_service_3' );
$icon_4 = get_field( 'icon_4' );
$name_of_service_4 = get_field( 'name_of_service_4' );
$icon_5 = get_field( 'icon_5' );
$name_of_service_5 = get_field( 'name_of_service_5' );
$icon_6 = get_field( 'icon_6' );
$name_of_service_6 = get_field( 'name_of_service_6' );

$service_page_1 = get_field( 'page_1' );
$service_page_2 = get_field( 'page_2' );
$service_page_3 = get_field( 'page_3' );
$service_page_4 = get_field( 'page_4' );
$service_page_5 = get_field( 'page_5' );
$service_page_6 = get_field( 'page_6' );

// Work Experience

$activation_work_experience = get_field( 'activation_work_experience' );
$company_name_1 = get_field( 'company_name_1' );
$the_years_of_your_work_in_this_company_1 = get_field( 'the_years_of_your_work_in_this_company_1' );
$description_work_1 = get_field( 'description_work_1' );

$company_name_2 = get_field( 'company_name_2' );
$the_years_of_your_work_in_this_company_2 = get_field( 'the_years_of_your_work_in_this_company_2' );
$description_work_2 = get_field( 'description_work_2' );

$company_name_3 = get_field( 'company_name_3' );
$the_years_of_your_work_in_this_company_3 = get_field( 'the_years_of_your_work_in_this_company_3' );
$description_work_3 = get_field( 'description_work_3' );

$company_name_4 = get_field( 'company_name_4' );
$the_years_of_your_work_in_this_company_4 = get_field( 'the_years_of_your_work_in_this_company_4' );
$description_work_4 = get_field( 'description_work_4' );

$company_name_5 = get_field( 'company_name_5' );
$the_years_of_your_work_in_this_company_5 = get_field( 'the_years_of_your_work_in_this_company_5' );
$description_work_5 = get_field( 'description_work_5' );

?>
<?php get_header(); ?>

 <!-- resume begin  -->
   <div class="main-content-wrapper main-area">
	 <div class="container">
	   <div class="blog">
		 <h1 class="main-title"><?php the_title(); ?></h1>
		 <div class="grey-line"></div>
		 <?php get_template_part('page-nav'); ?>
		 
		 <div class="resume-area">
		  <?php if ( $activate_education ) : ?>
			<h2 class="resume-title">Education</h2>
			
			<?php if ( $first_education_place ) : ?>
				<div class="study-place study-place-1">
					<h3><?php echo $first_education_place; ?></h3>
					<div class="time-period"><i class="fa fa-calendar"></i><?php echo $years_you_studied; ?></div>
					<p class="study-place-text"><?php echo $description_1; ?></p>
				</div>
			<?php endif; ?>

			<?php if ( $second_education_place ) : ?>
				<div class="study-place study-place-2">
					<h3><?php echo $second_education_place; ?></h3>
					<div class="time-period"><i class="fa fa-calendar"></i><?php echo $years_you_studied_2; ?></div>
					<p class="study-place-text"><?php echo $description_2; ?></p>
				</div>
			<?php endif; ?>

			<?php if ( $third_education_place ) : ?>
				<div class="study-place study-place-3">
					<h3><?php echo $third_education_place; ?></h3>
					<div class="time-period"><i class="fa fa-calendar"></i><?php echo $years_you_studied_3; ?></div>
					<p class="study-place-text"><?php echo $description_3; ?></p>
				</div>
			<?php endif; ?>

			<?php if ( $fourth_education_place ) : ?>
				<div class="study-place study-place-4">
					<h3><?php echo $fourth_education_place; ?></h3>
					<div class="time-period"><i class="fa fa-calendar"></i><?php echo $years_you_studied_4; ?></div>
					<p class="study-place-text"><?php echo $description_4; ?></p>
				</div>
			<?php endif; ?>

			<?php if ( $fifth_education_place ) : ?>
				<div class="study-place study-place-5">
					<h3><?php echo $fifth_education_place; ?></h3>
					<div class="time-period"><i class="fa fa-calendar"></i><?php echo $years_you_studied_5; ?></div>
					<p class="study-place-text"><?php echo $description_5; ?></p>
				</div>
			<?php endif; ?>

		  <?php endif; ?>
		  	<?php 
		  	  	echo '<style type="text/css">
		  				.skill-progress-bar-1 > div {
		  					width: '.$skill_level_in_percent_value_1.'%;
		  				}
		  				.skill-progress-bar-2 > div {
		  					width: '.$skill_level_in_percent_value_2.'%;
		  				}
		  				.skill-progress-bar-3 > div {
		  					width: '.$skill_level_in_percent_value_3.'%;
		  				}
		  				.skill-progress-bar-4 > div {
		  					width: '.$skill_level_in_percent_value_4.'%;
		  				}
		  				.skill-progress-bar-5 > div {
		  					width: '.$skill_level_in_percent_value_5.'%;
		  				}
		  				.skill-progress-bar-6 > div {
		  					width: '.$skill_level_in_percent_value_6.'%;
		  				}
		  				.skill-progress-bar-7 > div {
		  					width: '.$skill_level_in_percent_value_7.'%;
		  				}
		  				.skill-progress-bar-8 > div {
		  					width: '.$skill_level_in_percent_value_8.'%;
		  				}
		  				.skill-progress-bar-9 > div {
		  					width: '.$skill_level_in_percent_value_9.'%;
		  				}
		  				.skill-progress-bar-10 > div {
		  					width: '.$skill_level_in_percent_value_10.'%;
		  				}
		  		      </style>';
		  	 ?>
			<?php if ( $skills_activate ) : ?>
				<div class="skill-area">
					<h2 class="resume-title">Skills</h2>
					
					<?php if ( $skill_1 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_1; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_1; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-1"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_2 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_2; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_2; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-2"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_3 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_3; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_3; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-3"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_4 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_4; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_4; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-4"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_5 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_5; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_5; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-5"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_6 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_6; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_6; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-6"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_7 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_7; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_7; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-7"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_8 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_8; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_8; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-8"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_9 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_9; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_9; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-9"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

					<?php if ( $skill_10 ) : ?>
						<div class="skill-box">
							<h4 class="skill-name-box">
								<p class="skill-name"><?php echo $skill_10; ?></p>
								<p class="skill-percent skill-percent-1"><?php echo $skill_level_in_percent_value_10; ?>%</p>
							</h4>
							<div class="skill-progress-bar skill-progress-bar-10"><div class="inner-bar"></div></div>
						</div>
					<?php endif; ?>

				</div>
			<?php endif; ?>
			
			<?php if ( $activate_services ) : ?>
				<div class="features">
					<h2 class="resume-title">Services</h2>
					
					<?php if ( $name_of_service_1 ) : ?>
						<a href="<?php echo get_page_link($service_page_1[0]); ?>" class="feature feature-1">
							<i class="fa <?php echo $icon_1; ?>"></i>
							<p><?php echo $name_of_service_1; ?></p>
						</a>
					<?php endif; ?>

					<?php if ( $name_of_service_2 ) : ?>
						<a href="<?php echo get_page_link($service_page_2[0]); ?>" class="feature feature-2">
							<i class="fa <?php echo $icon_2; ?>"></i>
							<p><?php echo $name_of_service_2; ?></p>
						</a>
					<?php endif; ?>
					
					<?php if ( $name_of_service_3 ) : ?>
						<a href="<?php echo get_page_link($service_page_3[0]); ?>" class="feature feature-3">
							<i class="fa <?php echo $icon_3; ?>"></i>
							<p><?php echo $name_of_service_3; ?></p>
						</a>
					<?php endif; ?>
					
					<?php if ( $name_of_service_4 ) : ?>
						<a href="<?php echo get_page_link($service_page_4[0]); ?>" class="feature feature-4">
							<i class="fa <?php echo $icon_4; ?>"></i>
							<p><?php echo $name_of_service_4; ?></p>
						</a>
					<?php endif; ?>	

					<?php if ( $name_of_service_5 ) : ?>
						<a href="<?php echo get_page_link($service_page_5[0]); ?>" class="feature feature-5">
							<i class="fa <?php echo $icon_5; ?>"></i>
							<p><?php echo $name_of_service_5; ?></p>
						</a>
					<?php endif; ?>	

					<?php if ( $name_of_service_6 ) : ?>
						<a href="<?php echo get_page_link($service_page_6[0]); ?>" class="feature feature-6">
							<i class="fa <?php echo $icon_6; ?>"></i>
							<p><?php echo $name_of_service_6; ?></p>
						</a>
					<?php endif; ?>	

				</div>
				<div class="grey-line"></div>
			<?php endif; ?>
			
			<?php if ( $activation_work_experience ) : ?>
				<h2 class="resume-title">Work Experience</h2>
				<?php if ( $company_name_1 ) : ?>
					<div class="study-place work-place">
						<h3><?php echo $company_name_1; ?></h3>
						<div class="time-period"><i class="fa fa-calendar"></i><?php echo $the_years_of_your_work_in_this_company_1; ?></div>
						<p class="study-place-text"><?php echo $description_work_1; ?></p>
					</div>
				<?php endif; ?>

				<?php if ( $company_name_2 ) : ?>
					<div class="study-place work-place">
						<h3><?php echo $company_name_2; ?></h3>
						<div class="time-period"><i class="fa fa-calendar"></i><?php echo $the_years_of_your_work_in_this_company_2; ?></div>
						<p class="study-place-text"><?php echo $description_work_2; ?></p>
					</div>
				<?php endif; ?>

				<?php if ( $company_name_3 ) : ?>
					<div class="study-place work-place">
						<h3><?php echo $company_name_3; ?></h3>
						<div class="time-period"><i class="fa fa-calendar"></i><?php echo $the_years_of_your_work_in_this_company_3; ?></div>
						<p class="study-place-text"><?php echo $description_work_3; ?></p>
					</div>
				<?php endif; ?>

				<?php if ( $company_name_4 ) : ?>
					<div class="study-place work-place">
						<h3><?php echo $company_name_4; ?></h3>
						<div class="time-period"><i class="fa fa-calendar"></i><?php echo $the_years_of_your_work_in_this_company_4; ?></div>
						<p class="study-place-text"><?php echo $description_work_4; ?></p>
					</div>
				<?php endif; ?>

				<?php if ( $company_name_5 ) : ?>
					<div class="study-place work-place">
						<h3><?php echo $company_name_5; ?></h3>
						<div class="time-period"><i class="fa fa-calendar"></i><?php echo $the_years_of_your_work_in_this_company_5; ?></div>
						<p class="study-place-text"><?php echo $description_work_5; ?></p>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div> 
</div>

   <?php get_footer(); ?>

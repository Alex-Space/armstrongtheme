<?php 
function createOptionsPage() {
    register_post_type(
        'options',
        array(
            'labels' => array(
                'name' => 'Theme settings'
            ),
            'public' => true,
            'menu_icon' => 'dashicons-admin-tools',
            'menu_position' => 100,
            'capabilities' => array(
                'create_posts' => 'do_not_allow',
                'delete_published_posts' => false
            ),
            'map_meta_cap' => true,
            'supports' => array(
                'title'
            )
        )
    );

    function removeViewLink($actions) {
        if(get_post_type() === 'options') {
            unset( $actions['view']);
            unset( $actions['inline hide-if-no-js']);
        }
        return $actions;
    }
    add_filter('post_row_actions', 'removeViewLink', 10, 1);


    $checkSettingsPage = get_page_by_path('theme-settings', array(), 'options');
    if(empty($checkSettingsPage)) {
        wp_insert_post(
            array(
                'post_title' => 'Theme settings',
                'post_slug' => 'theme-settings',
                'post_type' => 'options',
                'post_status' => 'publish',
                'post_author' => 1
            )
        );
    }


    function getOptionsField($fieldName) {
        $optionsPageId = get_page_by_path('theme-settings', OBJECT, 'options')->ID;
        $field = get_field($fieldName, $optionsPageId);

        return $field;
    }
}
add_action('init', 'createOptionsPage');
?>
jQuery(document).ready(function($) {


	// prevent default actions
	$('.no-link').click(function(event) {
		event.preventDefault();
	});

	// remove all br's in gallery section
	$('.type-gallery .gallery br').remove();
	$('.type-gallery .gallery, .simple-gallery .gallery').bxSlider({
		mode: 'fade'
	});

	// burger-btn animation
	$('.burger-btn').click(function(event) {
		$('.burger-line-1').toggleClass('burger-line-1-active');
		$('.burger-line-2').toggleClass('burger-line-2-active');
		$('.burger-line-3').toggleClass('burger-line-3-active');
		$('.main-site-menu').slideToggle();
	});

	// fix profile info-boxes behavior
	function fixInfoboxSize () {
		
		var pageWidth = $('.profile-page').width() - 5;
		
		$('.personal-info > div').each(function() {
			//$(this).css('outline', '2px solid red');
			var firstSpan = $(this).children('span').first();
			var firstSpanWidth = $(this).children('span').first().width() + 30;
			var lastSpan = $(this).children('span').last();
			var lastSpanWidth = $(this).children('span').last().width() + 30;
			var sum = firstSpanWidth + lastSpanWidth + 20;

			if ( sum > pageWidth ) {
				firstSpan.css({
					'borderBottom': '0',
					'borderRight': '1px solid #ededed'
				});
				lastSpan.css('clear', 'both');
			} else {
				firstSpan.css({
					'borderRight': '0',
					'borderBottom': '1px solid #ededed'
				});
				lastSpan.css('clear', 'none');
			}
		});
	}

	fixInfoboxSize();
	
	$(window).resize(function(event) {
		fixInfoboxSize();
	});

	if ( !$('div').is('.left-form-box') ) {
		$('.comment-form-comment textarea').css('width', '100%');
	}
	
	// works portfolio
	var workContainerHeight = $('.works-post').height();
	$('.works-post').css( 'height', workContainerHeight );
	$('.works-navigation').on('click', 'a', function(event) {
		event.preventDefault();
		
		var NavElemClass = $(this).parent('li').attr('class');

		$('.all a').css('backgroundColor', 'transparent');
		
		$('.works-navigation li a').removeClass('active-work-tab');
		$(this).addClass('active-work-tab');

		$('.work-box').each(function() {
			if ( $(this).hasClass( NavElemClass) ) {
				$(this).show('slow');
				// .css({'position': 'static', 'display': 'inline-block'})
				//$(this).addClass('work-active-element');
			} else {
				$(this).hide('slow');
				// .css({'position': 'absolute', 'display': 'inline-block'})
				//$(this).removeClass('work-active-element');
			}
		});
	});

	// Social
	$('.prev-def').click(function(event) {
		event.preventDefault();
	});
}); // the end
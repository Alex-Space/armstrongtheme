<!DOCTYPE html>
  <meta name="keywords" content="" />
  <meta name="description" content="">
  <title><?php the_title(); ?></title>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1" />
  <meta name="viewport" content="minimal-ui">
  <!-- Apple device hack starts -->
	<meta name="format-detection" content="telephone=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
  <!-- Apple device hack ends -->
  <meta name="format-detection" content="telephone=no">
  
  <link rel="icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php
	$avatar_background_image = getOptionsField('avatar_background_image');
	$main_color_for_navigation_menu_elements = getOptionsField('main_color_for_navigation_menu_elements');
	$hover_effect_for_navigation_menu_elements = getOptionsField('hover_effect_for_navigation_menu_elements');
	$color_for_line_between_navigation_menu_elements = getOptionsField('color_for_line_between_navigation_menu_elements');
	$color_for_text_icons = getOptionsField('color_for_text_icons');
	$hover_effect_for_text_icons = getOptionsField('hover_effect_for_text_icons');
	$main_site_background_color = getOptionsField('main_site_background_color');
	$background_color_for_main_content_area = getOptionsField('background_color_for_main_content_area');
	$color_for_text_in_the_footer_area = getOptionsField('color_for_text_in_the_footer_area');
	$color_for_icons_in_the_footer_area = getOptionsField('color_for_icons_in_the_footer_area');
	$hover_effect_for_icons_in_the_footer_area = getOptionsField('hover_effect_for_icons_in_the_footer_area');
	$color_for_text_icons_in_meta_information = getOptionsField('color_for_text_icons_in_meta_information');
?>

<?php 
echo '<style>
		body {
			background: '.$main_site_background_color.';
		}
		.main-area {
			background: '.$background_color_for_main_content_area.';
		}
		.blog-meta-information div {
			color: '.$color_for_text_icons_in_meta_information.';
		}
		p.footer-left-box {
			color: '.$color_for_text_in_the_footer_area.';
		}
		.footer-right-box a {
			color: '.$color_for_icons_in_the_footer_area.';
		}
		.footer-right-box a:hover {
			color: '.$hover_effect_for_icons_in_the_footer_area.';
		}
		.side-bar-area .menu li {
			background: '.$main_color_for_navigation_menu_elements.';
			border-bottom: 1px solid '.$color_for_line_between_navigation_menu_elements.';
		}
		.side-bar-area .menu li:hover {
			background: '.$hover_effect_for_navigation_menu_elements.';
		}
		.menu li.current-menu-item {
			background: '.$main_color_for_navigation_menu_elements.';
		}
		.menu li.current-menu-item:before,
		.side-bar-area .menu li.current-menu-item a {
			color: '.$color_for_text_icons.';
		}
		.side-bar-area .menu li a {
			color: '.$color_for_text_icons.';
		}
		.side-bar-area .menu li a:hover,
		.menu li.current-menu-item:hover:before,
		.side-bar-area .menu li:hover:before {
			color: '.$hover_effect_for_text_icons.';
		}
		.side-bar-area .menu li:hover a {
			color: '.$hover_effect_for_text_icons.';
		}
	  </style>'; 
?>

<?php if ( getOptionsField('avatar_background_image') ) : ?>
  <?php echo '<style>
				.avatar-section {
				  background: url("'.$avatar_background_image.'") no-repeat;
				}
			  </style>'; ?>
<?php endif; ?>

<?php get_page_navigation_styles(); ?>

<div class="main-container">
<!-- side-bar begin  -->
  <div class="side-bar-area">
	<div class="container">
	  <div class="side-bar">
		<div class="avatar-section">
		  <div class="burger-btn">
			<div class="burger-line-1"></div>
			<div class="burger-line-2"></div>
			<div class="burger-line-3"></div>
		  </div>
		</div>
		<div class="main-site-menu">
		  <?php wp_nav_menu( array( 'header_menu' => 'Main menu' ) ); ?>
		</div>
	  </div>
	</div>
  </div>
<!-- side-bar end  -->
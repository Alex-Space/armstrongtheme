<?php 
/*
Template Name: Profile
*/ 
?>
<?php get_header(); ?>
<?php 
$owner_name = get_field('your_name');
$about_owner = get_field('about_you');
$owner_profession = get_field('your_profession');
$owner_date_of_birth = get_field('date_of_birth');
$owner_email = get_field('e-mail');
$owner_address = get_field('address');
$owner_phone_number = get_field('phone');
$owner_website = get_field('website');
 ?>

<!-- profile begin  -->
  <div class="main-content-wrapper main-area contact-area">
	<div class="container">
	  <div class="profile-page">

	  	<?php if ( $owner_name && $owner_profession ) : ?>
			<h1 class="profile-main-title">
				Hello, I am <span class="owner-name"><?php echo $owner_name; ?></span><br>
				<span class="owner-profession">Designer and Front-end Developer</span>
			</h1>
		<?php else : ?>
			<h1 class="profile-main-title">
				Hello, I am <span class="owner-name">Robb Armstrong</span><br>
				<span class="owner-profession"><?php echo $owner_profession; ?></span>
			</h1>
		<?php endif; ?>

		<?php if ( $about_owner ) : ?>
			<p class="about-me-text"><?php echo $about_owner; ?></p>
		<?php else : ?>
			<p class="about-me-text">
				I have ten years experience as a web/interface designer. I have a love of clean, elegant styling, and I have lots of experience in the production of CSS and (X)HTML for modern websites. I have a reasonable grasp of using JavaScript frameworks, specifically jQuery.
			</p>
		<?php endif; ?>

		<h2 class="personal-info-title">Personal Info</h2>
		<div class="personal-info">
			
			<?php if ( $owner_name ) : ?>
				<div class="name-info">
					<span>name</span>
					<span><?php echo $owner_name; ?></span>
				</div>
			<?php else : ?>
				<div class="name-info">
					<span>name</span>
					<span>Robb Armstrong</span>
				</div>
			<?php endif; ?>

			<?php if ( $owner_date_of_birth ) : ?>
				<div class="date-of-birth-info">
					<span>date of birth</span>
					<span><?php echo $owner_date_of_birth; ?></span>
				</div>
			<?php else : ?>
				<div class="date-of-birth-info">
					<span>date of birth</span>
					<span>September 06, 1976</span>
				</div>
			<?php endif; ?>

			<?php if ( $owner_email ) : ?>
				<div class="e-mail-info">
					<span>e-mail</span>
					<span><?php echo $owner_email; ?></span>
				</div>
			<?php else : ?>
				<div class="e-mail-info">
					<span>e-mail</span>
					<span>info@yourdomain.com</span>
				</div>
			<?php endif; ?>

			<?php if ( $owner_address ) : ?>
				<div class="address-info">
					<span>address</span>
					<span><?php echo $owner_address; ?></span>
				</div>
			<?php else : ?>
				<div class="address-info">
					<span>address</span>
					<span>121 King St, Melbourne VIC</span>
				</div>
			<?php endif; ?>
			
			<?php if ( $owner_phone_number ) : ?>
				<div class="phone-info">
					<span>phone</span>
					<span><?php echo $owner_phone_number; ?></span>
				</div>
			<?php else : ?>
				<div class="phone-info">
					<span>phone</span>
					<span>012-3456-7890</span>
				</div>
			<?php endif; ?>

			<?php if ( $owner_website ) : ?>
				<div class="website-info">
					<span>website</span>
					<span><a href="http://<?php echo $owner_website; ?>"><?php echo $owner_website; ?></a></span>
				</div>
			<?php else : ?>
				<div class="website-info">
					<span>website</span>
					<span><a href="http://wpmonsters.org/">wpmonsters.org</a></span>
				</div>
			<?php endif; ?>
		</div>
	</div>
  <?php get_footer(); ?>

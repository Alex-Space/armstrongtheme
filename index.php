<?php 
/*
Template Name: Blog
*/ 
?>
<?php 

 


?>
<?php get_header(); ?>

<?php 
$activate_banner_value = get_field('activate_banner');
$link_for_banner = get_field('link_for_banner');
$text_first_banner = get_field('text_first_banner');
$quote_activation = get_field('quote_activation');
$quote_of_great_human = get_field('quote_of_great_human');
$name_of_quote_owner = get_field('name_of_quote_owner');
// pr(); exit;
?>
  
  <!-- blog begin  -->
    <div class="main-content-wrapper main-area">
      <div class="container">
        <div class="blog">
          <h1 class="main-title"><?php the_title(); ?></h1>
          <div class="grey-line"></div>
          <?php get_template_part('page-nav'); ?>
          <?php 
            $query = new WP_Query( array( 'posts_per_page'=> 1 ) );
            if (is_object($query)) : while ($query->have_posts()) : $query->the_post(); ?>
            
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
              <div class="article-header">
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

                <?php get_template_part('blog_meta_info'); ?>
              </div>
              <?php if ( has_post_thumbnail() ) : ?>
                <div class="blog-thumbnail">
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                </div>
              <?php else : ?>
                <div class="blog-thumbnail blog-thumbnail-default">
                  <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo( 'template_url' ) ?>/img/small/gallery-sample.jpg"></a>
                </div>
              <?php endif; ?>
              
              <?php the_content(); ?>
            </div> 
          <?php endwhile; ?>
          <?php endif; ?>
          <?php 
          if ( $activate_banner_value ) : ?>
          <!-- blog-banner begin  -->
            <div class="blog-banner-area">
              <div class="container">
                <a target="_blank" href="<?php echo $link_for_banner; ?>" class="blog-banner blog-banner-yellow">
                 <p><i class="link-icon"></i><?php echo $text_first_banner; ?></p>
                </a>
              </div>
            </div>
          <!-- blog-banner end  -->
          <?php endif; ?>
          <?php 
            $query = new WP_Query( array( 'posts_per_page'=> 1, 'post_type' => 'gallery', 'offset' => 0) );
            if (is_object($query)) : while ($query->have_posts()) : $query->the_post(); ?>
            
            <div class="blog-thumbnail">
              <?php 
              if ( has_post_thumbnail() ) {
                the_post_thumbnail();
              } ?>
            </div>
            
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
              <div class="article-header">
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <?php get_template_part('blog_meta_info'); ?>
              </div>
              <?php the_content(); ?>
            </div> 
          <?php endwhile; ?>
          <?php endif; ?>

          <?php if ( $quote_activation ) : ?>
          <!-- blog-banner begin  -->
            <div class="blog-banner-area blog-banner-area-with-border">
              <div class="container">
                <div class="blog-banner blog-banner-black">
                  <a class="no-link" href="#">
                    <?php echo $quote_of_great_human; ?>
                    <i class="commas-icon"></i>
                    <span class="quote-owner"><?php echo $name_of_quote_owner; ?></span>
                  </a>
                </div>
              </div>
            </div>
          <!-- blog-banner end  -->
          <?php endif; ?>

          <?php 
            $query = new WP_Query( array( 'posts_per_page'=> 1, 'offset' => 1 ) );
            if (is_object($query)) : while ($query->have_posts()) : $query->the_post(); ?>
            
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
              <div class="article-header">
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

                <?php get_template_part('blog_meta_info'); ?>
              </div>
              <div class="blog-thumbnail">
                <?php 
                if ( has_post_thumbnail() ) {
                  the_post_thumbnail();
                } ?>
              </div>
              <?php the_content(); ?>
            </div> 
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    <?php get_footer(); ?>
  

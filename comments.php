<?php

if ( post_password_required() ) {
	return;
}

function mytheme_comment($comment, $args, $depth){
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	<div id="comment-<?php comment_ID(); ?>">
	<div class="comment-author vcard">
		<?php echo get_avatar( $comment, $size='48', $default='<path_to_url>' ); ?>

		
	</div>

	<div class="comment-meta commentmetadata">
		<cite class="fn"><?php echo get_comment_author_link() ?></cite>
		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf( '%1$s в %2$s', get_comment_date(),  get_comment_time()) ?></a>
		<?php edit_comment_link('(Редактировать)', '  ', '') ?>
		<?php comment_text() ?>
		<div class="reply">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
	</div>
	
		</div>
<?php
}
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( '1 Comment', '%1$s Comments', get_comments_number(), 'comments title'),
					number_format_i18n( get_comments_number() ), get_the_title() );
			?>
		</h2>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 62,
					'reply_text'  => 'reply',
					'max_depth'   => 3,
					'reverse_top_level' => false,
					'callback'    => 'mytheme_comment'
				) );
			?>
		</ol><!-- .comment-list -->

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.'); ?></p>
	<?php endif; ?>
	<?php 
	$fields =  array(
		'author' => '<div class="left-form-box"><p class="comment-form-author">' .	'<input placeholder="name" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
		'email'  => '<p class="comment-form-email">' . '<input placeholder="e-mail" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
		'url'    => '<p class="comment-form-url">' . '<input placeholder="website" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p></div>',
						);
			 ?>
	<?php comment_form( array(
		'title_reply'          => __( 'Leave a comment' ),
		'title_reply_to'       => __( 'Leave a comment to %s' ),
		'cancel_reply_link'    => __( 'Cancel comment' ),
		'label_submit'         => __( 'Post Comment' ),
		'fields' => apply_filters( 'comment_form_default_fields', $fields ),
		'comment_notes_before' => '',
		'label_submit'         => 'Add a comment',
		'id_submit'            => 'comment-sub'
		
	) ); ?>

</div><!-- .comments-area -->

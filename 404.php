<?php get_header(); ?>

<!-- 404 begin  -->
  <div class="main-content-wrapper main-area">
    <div class="container">
      <div class="blog error-page">
        <h1 class="main-title">Page not found</h1>
        <div class="grey-line"></div>
        <?php get_template_part('page-nav'); ?>
        <div class="error-box">
          <h1 class="error-not-found-page-title">404</h1>
        </div>
        </div> 
      </div>

  <?php get_footer(); ?>

<?php 
/*
Template Name: Contact
*/ 
?>
<?php get_header(); ?>
<?php 
	$map = get_field('map');
	$activate_contacts_info = get_field('activate_contacts_info');
	$your_adress = get_field('your_adress');
	$phone_number = get_field('phone_number');
	$mail_contact = get_field('e-mail_contact');
	$shortcode_for_contact_from = get_field('shortcode_for_contact_from');
?>
<!-- contact begin  -->
  <div class="main-content-wrapper main-area contact-area">
	<div class="container">
	  <div class="blog contact-page">
		<h1 class="main-title"><?php the_title(); ?></h1>
		<div class="grey-line"></div>
		<?php get_template_part('page-nav'); ?>
		
		<?php if ( $map ) : ?>
			<div class="map-area-empty">
				
				<?php 
				  echo '<script src="https://maps.googleapis.com/maps/api/js"></script>
				  <script>
				    function initialize() {
				      var mapCanvas = document.getElementById("map-canvas");
				      var mapOptions = {
				        center: new google.maps.LatLng('.$map['lat'].', '.$map['lng'].'),
				        zoom: 8,
				        mapTypeId: google.maps.MapTypeId.ROADMAP
				      }
				      var map = new google.maps.Map(mapCanvas, mapOptions)
				    }
				    google.maps.event.addDomListener(window, "load", initialize);
				  </script>';
				  echo '<div id="map-canvas"></div>';
				 ?>

			</div>
		<?php else : ?>
			<div class="map-area"></div>
		<?php endif; ?>

		<h2 class="thin-title">Contact info</h2>
		
		<div class="contact-message-area">
		  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?> 

		  <?php endwhile; ?>
		  <?php endif; ?>
		</div>
		
		<?php if ( $activate_contacts_info ) : ?>
			<div class="contact-info-area">
				
				<?php if ( $your_adress ) : ?>
					<div class="contact-adress">
					  	<i class="home-icon"></i>
					  	<?php echo $your_adress; ?>
					</div>
				<?php endif; ?>

				<?php if ( $phone_number ) : ?>
					<div class="contact-phone">
						<i class="phone-icon"></i>
					<?php echo $phone_number; ?>
					</div>
				<?php endif; ?>

				<?php if ( $mail_contact ) : ?>
					<div class="contact-email">
					  	<i class="small-mail-icon"></i>
					  	<a class="mail-contact" href="mailto:<?php echo $mail_contact; ?>"><?php echo $mail_contact; ?></a>
					</div>
				<?php endif; ?>

			</div>
		<?php endif; ?>
		
		<?php if ( $shortcode_for_contact_from ) : ?>
			<?php echo do_shortcode( $shortcode_for_contact_from ); ?>
		<?php endif; ?>
		
		<!-- <div class="contact-form-area">
			<h2 class="thin-title contact-form-title">Send us a message</h2>
			<form class="main-contact-form">
				<div class="left-form-box">
					<input type="text" name="name" placeholder="name">
					<input type="text" name="e-mail" placeholder="email">
					<input type="text" name="website" placeholder="website">
				</div>
				<div class="right-form-box">
					<textarea class="message-field"></textarea>
					<button class="yellow-btn form-btn" type="submit">Send Message</button>
				</div>
			</form>
		</div> -->
	  </div>
	</div>
  <?php get_footer(); ?>

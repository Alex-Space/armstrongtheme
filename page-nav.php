<div class="page-navigation">
  <p class="page-navigation-text">Go to next / previous page</p>
  <div class="navigation-link-box">
    <a class="prev-page-link" href="<?php echo get_page_navigation( 'prev' ); ?>"></a>
    <a class="next-page-link" href="<?php echo get_page_navigation( 'next' ); ?>"></a>
  </div>
</div>